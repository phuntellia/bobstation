/*
	Disembowelment
*/
/datum/wound/disembowel
	name = "Disembowelment"
	desc = "Patient's limb has been violently avulsioned, to the point of large chunks of flesh and organs getting lost."
	viable_zones = ALL_BODYPARTS_MINUS_EYES
	wound_type = WOUND_LIST_DISEMBOWEL
	severity = WOUND_SEVERITY_LOSS
	ignore_preexisting = TRUE
	threshold_minimum = 180
	status_effect_type = null
	scarring_descriptions = list("has a large, wide and slithering keloid scar", "is a gruesome patchwork of badly healed flesh and skin", "has a large series of connected hypertrophic scars")
	biology_required = list()
	required_status = null

/datum/wound/disembowel/proc/apply_disembowel(obj/item/bodypart/L, wounding_type = WOUND_SLASH, silent = FALSE)
	if(!istype(L) || !L.owner || !(L.body_zone in viable_zones) || isalien(L.owner) || !L.can_disembowel())
		qdel(src)
		return

	if(ishuman(L.owner))
		var/mob/living/carbon/human/H = L.owner
		if((required_status & BODYPART_ORGANIC) && !L.is_organic_limb())
			qdel(src)
			return
		else if((required_status & BODYPART_ROBOTIC) && !L.is_robotic_limb())
			qdel(src)
			return
		
		for(var/biology_flag in biology_required)
			if(!(biology_flag in H.dna.species.species_traits))
				qdel(src)
				return

	occur_text = "is slashed deep through it's flesh, letting organs and bits of flesh fall out"
	switch(wounding_type)
		if(WOUND_BLUNT)
			occur_text = "is crushed through it's wounds, all organs inside gruesomely fall out"
			if(L.is_robotic_limb())
				occur_text = "is shattered through it's exoskeleton, spitting out internal components"
		if(WOUND_SLASH)
			occur_text = "is slashed deep through it's flesh, letting organs and bits of flesh fall out"
			if(L.is_robotic_limb())
				occur_text = "is slashed through it's exoskeleton, internal components rapidly falling out"
		if(WOUND_PIERCE)
			occur_text = "is deeply pierced through, internal organs easily falling out of the gaping wound"
			if(L.is_robotic_limb())
				occur_text = "is deeply pierced through, internal components easily falling out of the gaping hole"
		if(WOUND_BURN)
			occur_text = "gets a hole burned through it, slightly charred organs falling out"
			if(L.is_robotic_limb())
				occur_text = "gets a critical amount of metal molten, opening a gaping hole from which slightly components fall through"

	var/mob/living/carbon/poorsod = L.owner
	if(prob(50 - GET_STAT_LEVEL(poorsod, end)))
		poorsod.confused += max(0, 25 - GET_STAT_LEVEL(poorsod, end))
	if(prob(35 - GET_STAT_LEVEL(poorsod, end)))
		poorsod.vomit(15, 15, 25)
	if(prob(60 - GET_STAT_LEVEL(poorsod, end)))
		poorsod.death_scream()
	
	var/msg = "<b><span class='danger'>[poorsod]'s [fake_body_zone ? parse_zone(fake_body_zone) : L.name] [occur_text]!</span></b>"
	
	if(!silent)
		poorsod.visible_message(msg, "<span class='userdanger'>Your [fake_body_zone ? parse_zone(fake_body_zone) : L.name] [occur_text]!</span>")
	
	//apply the blood gush effect
	if(wounding_type != WOUND_BURN && L.owner)
		var/direction = L.owner.dir
		direction = turn(direction, 180)
		var/bodypart_turn = 0 //relative north
		if(L.body_zone in list(BODY_ZONE_L_ARM, BODY_ZONE_L_LEG, BODY_ZONE_PRECISE_L_FOOT, BODY_ZONE_PRECISE_L_HAND))
			bodypart_turn = -90 //relative east
		else if(L.body_zone in list(BODY_ZONE_R_ARM, BODY_ZONE_R_LEG, BODY_ZONE_PRECISE_R_FOOT, BODY_ZONE_PRECISE_R_HAND))
			bodypart_turn = 90 //relative west
		direction = turn(direction, bodypart_turn)
		var/dist = rand(3, 5)
		var/turf/targ = get_ranged_target_turf(L.owner, direction, dist)
		if(istype(targ) && dist > 0 && ((L.owner.mob_biotypes & MOB_ORGANIC) || (L.owner.mob_biotypes & MOB_HUMANOID)) && L.owner.needs_heart() && !L.owner.is_asystole() && (ishuman(L.owner) ? !(NOBLOOD in L.owner.dna?.species?.species_traits) : TRUE))
			var/obj/effect/decal/cleanable/blood/hitsplatter/B = new(L.owner.loc, L.owner.get_blood_dna_list())
			B.add_blood_DNA(L.owner.get_blood_dna_list())
			B.GoTo(targ, dist)

	second_wind()
	log_wound(poorsod, src)
	qdel(src)
	var/kaplosh_sound = 'modular_skyrat/sound/gore/dissection.ogg'
	if(length(L.dismember_sounds))
		kaplosh_sound = pick(L.dismember_sounds)
	if(L.is_robotic_limb())
		kaplosh_sound = 'modular_skyrat/sound/effects/crowbarhit.ogg'
	playsound(L.owner, kaplosh_sound, 80, 0)
	L.disembowel(dam_type = (wounding_type == WOUND_BURN ? BURN : BRUTE), silent = TRUE, wound = TRUE, wounding_type = wounding_type)
	qdel(src)

/datum/wound/slash/critical/incision/disembowel
	name = "Disembowelment"
	desc = "Patient's limb has been violently avulsioned, to the point of large chunks of flesh and organs getting lost."
	treat_text = "Immediate surgical closure of the wound, as well as reimplantation of lost organs."
	examine_desc = "has a wide and gaping wound"
	viable_zones = ALL_BODYPARTS_MINUS_EYES
	severity = WOUND_SEVERITY_CRITICAL
	wound_type = WOUND_LIST_DISEMBOWEL
	ignore_preexisting = TRUE
	max_per_type = 4
	threshold_penalty = 80
	demotes_to = null
	threshold_minimum = 180
	status_effect_type = /datum/status_effect/wound/slash/critical
	scarring_descriptions = list("is several skintone shades paler than the rest of the body", "is a gruesome patchwork of artificial flesh", "has a large series of attachment scars at the articulation points")
	required_status = BODYPART_ORGANIC
	biology_required = list()
	pain_amount = 40 //Just absolutely unbearable. Will send you into shock most of the time.
	infection_chance = 90
	infection_rate = 6
	occur_text = null
	initial_flow = 4.25
	minimum_flow = 4
	clot_rate = 0
	descriptive = "The limb is disemboweled!"
	wound_flags = (MANGLES_SKIN | MANGLES_MUSCLE | VISIBLE_THROUGH_CLOTHING)
	var/datum/component/storage/concrete/organ/our_component

/datum/wound/slash/critical/incision/disembowel/get_examine_description(mob/user)
	if(limb.body_zone in list(BODY_ZONE_HEAD, BODY_ZONE_PRECISE_NECK))
		return "<span class='deadsay'>[..()]</span>"
	. = ..()

/datum/wound/slash/critical/incision/disembowel/Destroy()
	var/datum/component/storage/concrete/organ/ST = victim?.GetComponent(/datum/component/storage/concrete/organ)
	if(ST)
		ST.accessible = null
	our_component = null
	. = ..()

/datum/wound/slash/critical/incision/disembowel/apply_wound(obj/item/bodypart/L, silent, datum/wound/old_wound, smited)
	descriptive = "\The [L.name] is disemboweled!"
	switch(L.body_zone)
		if(BODY_ZONE_HEAD)
			initial_flow *= 1
			minimum_flow *= (1/4)
		if(BODY_ZONE_PRECISE_NECK)
			initial_flow *= (6/4)
			minimum_flow *= (1/4)
		if(BODY_ZONE_CHEST)
			initial_flow *= (5/4)
			minimum_flow *= (1/4)
		if(BODY_ZONE_PRECISE_GROIN)
			initial_flow *= (5/4)
			minimum_flow *= (1/4)
		if(BODY_ZONE_L_ARM)
			initial_flow *= (3/4)
			minimum_flow *= (1/4)
		if(BODY_ZONE_R_ARM)
			initial_flow *= (3/4)
			minimum_flow *= (1/4)
		if(BODY_ZONE_PRECISE_L_HAND)
			initial_flow *= (1/2)
			minimum_flow *= (1/3)
		if(BODY_ZONE_PRECISE_R_HAND)
			initial_flow *= (1/2)
			minimum_flow *= (1/3)
		if(BODY_ZONE_L_LEG)
			initial_flow *= (3/4)
			minimum_flow *= (1/4)
		if(BODY_ZONE_R_LEG)
			initial_flow *= (3/4)
			minimum_flow *= (1/4)
		if(BODY_ZONE_PRECISE_L_FOOT)
			initial_flow *= (1/2)
			minimum_flow *= (1/3)
		if(BODY_ZONE_PRECISE_R_FOOT)
			initial_flow *= (1/2)
			minimum_flow *= (1/3)
	our_component = L.owner.GetComponent(/datum/component/storage/concrete/organ)
	if(our_component)
		our_component.bodypart_affected = L
		our_component.drop_all_on_deconstruct = FALSE
		our_component.silent = TRUE
		our_component.accessible = TRUE
		our_component.update_insides()
	. = ..()

/datum/wound/mechanical/slash/critical/incision/disembowel
	name = "Disemboweled"
	desc = "Patient's limb has been violently shredded, to the point of large chunks of metal and components getting lost."
	treat_text = "Immediate welding of the wound, as well as reattachment of lost components."
	examine_desc = "has a wide and gaping tear"
	viable_zones = ALL_BODYPARTS_MINUS_EYES
	severity = WOUND_SEVERITY_CRITICAL
	wound_type = WOUND_LIST_DISEMBOWEL
	initial_flow = 2
	minimum_flow = 0.5
	clot_rate = 0
	max_per_type = 4
	threshold_penalty = 80
	demotes_to = null
	threshold_minimum = 180
	status_effect_type = /datum/status_effect/wound/slash/critical
	scarring_descriptions = list("is several skintone shades paler than the rest of the body", "is a gruesome patchwork of artificial flesh", "has a large series of attachment scars at the articulation points")
	required_status = BODYPART_ROBOTIC
	biology_required = list()
	pain_amount = 40 //Just absolutely unbearable. Will send you into shock most of the time.
	occur_text = null
	descriptive = "The limb is disemboweled!"
	wound_flags = (MANGLES_SKIN | MANGLES_MUSCLE | VISIBLE_THROUGH_CLOTHING)
	var/datum/component/storage/concrete/organ/our_component

/datum/wound/mechanical/slash/critical/incision/disembowel/get_examine_description(mob/user)
	if(limb.body_zone in list(BODY_ZONE_HEAD, BODY_ZONE_PRECISE_NECK))
		return "<span class='deadsay'>[..()]</span>"
	. = ..()

/datum/wound/mechanical/slash/critical/incision/disembowel/Destroy()
	var/datum/component/storage/concrete/organ/ST = victim?.GetComponent(/datum/component/storage/concrete/organ)
	if(ST)
		qdel(ST)
	QDEL_NULL(our_component)
	. = ..()

/datum/wound/mechanical/slash/critical/incision/disembowel/apply_wound(obj/item/bodypart/L, silent, datum/wound/old_wound, smited)
	descriptive = "\The [L.name] is disemboweled!"
	switch(L.body_zone)
		if(BODY_ZONE_HEAD)
			initial_flow *= 1
			minimum_flow *= (1/4)
		if(BODY_ZONE_PRECISE_NECK)
			initial_flow *= (6/4)
			minimum_flow *= (1/4)
		if(BODY_ZONE_CHEST)
			initial_flow *= (5/4)
			minimum_flow *= (1/4)
		if(BODY_ZONE_PRECISE_GROIN)
			initial_flow *= (5/4)
			minimum_flow *= (1/4)
		if(BODY_ZONE_L_ARM)
			initial_flow *= (3/4)
			minimum_flow *= (1/4)
		if(BODY_ZONE_R_ARM)
			initial_flow *= (3/4)
			minimum_flow *= (1/4)
		if(BODY_ZONE_PRECISE_L_HAND)
			initial_flow *= (1/2)
			minimum_flow *= (1/3)
		if(BODY_ZONE_PRECISE_R_HAND)
			initial_flow *= (1/2)
			minimum_flow *= (1/3)
		if(BODY_ZONE_L_LEG)
			initial_flow *= (3/4)
			minimum_flow *= (1/4)
		if(BODY_ZONE_R_LEG)
			initial_flow *= (3/4)
			minimum_flow *= (1/4)
		if(BODY_ZONE_PRECISE_L_FOOT)
			initial_flow *= (1/2)
			minimum_flow *= (1/3)
		if(BODY_ZONE_PRECISE_R_FOOT)
			initial_flow *= (1/2)
			minimum_flow *= (1/3)
	our_component = L.owner.GetComponent(/datum/component/storage/concrete/organ)
	if(our_component)
		our_component.bodypart_affected = L
		our_component.drop_all_on_deconstruct = FALSE
		our_component.silent = TRUE
		our_component.accessible = TRUE
		our_component.update_insides()
	. = ..()
